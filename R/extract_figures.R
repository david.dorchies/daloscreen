#' Extract figures
#'
#' @param img
#' @param crop
#' @param n_figures
#' @param resized_height
#' @param quantize
#'
#' @return
#' @export
#'
#' @examples
extract_figures <- function(file,
                            cfg,
                            forNN = TRUE) {
  crop <- cfg$crop[[dirname(file)]]
  bNotBlankDetected <- FALSE
  l <- list()
  for (i in cfg$n_figures) {
    tmp_file <- tempfile(fileext = ".jpg")
    magick::image_read(file) %>%
      magick::image_crop(sprintf("%fx%f+%f+%f",
                         crop$width,
                         crop$width * cfg$aspect_ratio,
                         crop$left + crop$width * (i - 1),
                         crop$top)) %>%
      # sharpening the image along edges https://ropensci.org/blog/2017/11/02/image-convolve/
      magick::image_convolve('DoG:0,0,2', scaling = '100, 100%') %>%
      magick::image_resize(
        magick::geometry_size_pixels(
          width = cfg$NN$width,
          height = round(cfg$NN$width * cfg$aspect_ratio),
          preserve_aspect = FALSE
        )
      ) %>%
      magick::image_write(path = tmp_file, format = "jpeg")

    df <- load_df_image(tmp_file, cfg)
    attr(df, "file") <- file
    dfGrad <- as.cimg(df, dims = c(max(df$x), max(df$y), 1, 1)) %>%
      imgradient("xy") %>% enorm %>% as.data.frame
    df$grad <- dfGrad$value
    if (forNN) {
      df$raw <- df$value
      df$value <- ecdf(df$value)(df$value)
      value_range <- quantile(df$raw, cfg$blank$min_range$quantiles)
      if (!bNotBlankDetected &
          i %in% cfg$blank$n_figures &
          abs(diff(value_range)) < cfg$blank$min_range$threshold) {
        df$value <- 1
      } else {
        bNotBlankDetected <- TRUE
      }
      df$value <- round(df$value * cfg$img$color_depth) / cfg$img$color_depth
    }
    df$i <- i
    l[[i]] <- df
  }
  do.call(rbind, l)
}


